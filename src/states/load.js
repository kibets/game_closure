import LStorageManager from "../utils/LStorageManager";
import SoundManager from "../utils/SoundManager";
import MusicManager from "../utils/MusicManager";

export default {
    loadingLabel() {
        let loading = game.add.sprite(game.world.centerX, game.world.centerY + 150, "loading");
        loading.anchor.setTo(0.5, 0.5);
        loading.scale.setTo(0.5);

        let bar = game.add.sprite(game.world.centerX, game.world.centerY + 87, "loading_bar");
        bar.anchor.setTo(0.5, 1);

        game.load.setPreloadSprite(bar, 1);
    },

    preload() {
        this.loadingLabel();

        this._loadArt();
        this._loadSounds();
        this._loadMusic();
        this._loadLevels();
    },

    create() {
        game.storage = new LStorageManager();
        game.sounder = new SoundManager();
        game.music = new MusicManager();

        game.state.start("play");
    },

    _loadArt() {
        game.load.image('logo', 'assets/logo.png');

        game.load.image('item_frame', 'assets/item_frame.png');
        game.load.image('item_frame_f', 'assets/item_frame_f.png');
        game.load.image('item_stand', 'assets/item_stand.png');
        game.load.image('item_stand_f', 'assets/item_stand_f.png');
        game.load.image('item_top', 'assets/item_top.png');
        game.load.image('item_top_f', 'assets/item_top_f.png');

        game.load.image('arrow', 'assets/arrow.png');

        game.load.image('item_shadow', 'assets/item_shadow.png');

        game.load.image('tree_01', 'assets/tree_01.png');
        game.load.image('tree_02', 'assets/tree_02.png');
        game.load.image('tree_03', 'assets/tree_03.png');
        game.load.image('tree_04', 'assets/tree_04.png');
        game.load.image('tree_05', 'assets/tree_05.png');
        game.load.image('tree_06', 'assets/tree_06.png');
        game.load.image('tree_07', 'assets/tree_07.png');
        game.load.image('tree_08', 'assets/tree_08.png');
        game.load.image('tree_09', 'assets/tree_09.png');
        game.load.image('tree_10', 'assets/tree_10.png');

        game.load.image('grass_01', 'assets/grass_01.png');
        game.load.image('grass_02', 'assets/grass_02.png');
        game.load.image('grass_03', 'assets/grass_03.png');
    },

    _loadSounds() {
        game.load.audio('ring_01', 'assets/sounds/ring_01.ogg');
        game.load.audio('plant_01', 'assets/sounds/plant_01.ogg');
        game.load.audio('upgrade_01', 'assets/sounds/upgrade_01.ogg');
        game.load.audio('click_01', 'assets/sounds/click_01.ogg');
        game.load.audio('click_02', 'assets/sounds/click_02.ogg');
        game.load.audio('click_03', 'assets/sounds/click_03.ogg');
    },

    _loadMusic() {
        game.load.audio('forest', 'assets/music/forest.ogg');
    },

    _loadLevels() {
        game.load.json('game_01', 'assets/levels/game_01.json');
    }
};
