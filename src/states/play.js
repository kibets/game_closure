import GameState from "../logic/GameState";
import GameView from "../view/GameView";
import GameController from "../controller/GameController";

export default {
    create() {
        this._startLevel(game.cache.getJSON('game_01'));

        game.music.loop('forest');
    },

    update() {
        this._controller.updateGame();
    },

    render() {
        // dev
        game.debug.text('FPS: ' + (game.time.fps || '--'), 4, game.canvas.height - 24);
    },

    _startLevel(level) {
        let gameState = new GameState(level);
        let gameView = new GameView(0, 0);

        let loaded = game.storage.loadGameState();
        gameState.deserialize(loaded);

        this._controller = new GameController(gameState, gameView);

        let mainGroup = game.add.group(undefined, "All");

        mainGroup.add(gameView);
    }

};
