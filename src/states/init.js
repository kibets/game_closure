export default {
    init() {
        // itch.io iframe focus fix
        game.input.mspointer.capture = false;

        game.stage.backgroundColor = "#eee9ce"
    },

    preload() {
        game.time.advancedTiming = true;
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.setResizeCallback(function () {
            game.scale.setMaximum();
        });

        game.load.image('loading', 'assets/loading.png');
        game.load.image('loading_bar', 'assets/loading_bar.png');
    },

    create() {
        game.state.start('load');
    }
};
