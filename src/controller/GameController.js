export default class GameController {
    constructor(gameState, gameView) {
        this._gameState = gameState;
        this._gameView = gameView;

        this._initGameView();
    }

    unlockBusiness(id) {
        this._gameState.unlockBusiness(id);
    }

    upgradeBusiness(id) {
        this._gameState.upgradeBusiness(id);
    }

    automateBusiness(id) {
        this._gameState.automateBusiness(id);
    }

    startBusiness(id) {
        this._gameState.startBusiness(id);
    }

    updateGame() {
        this._gameState.simulateNow();

        this._updateGameView();
        this._persist();
    }

    resetGameProgress() {
        this._gameState.resetGameProgress();
        this._persist();
    }

    _initGameView() {
        this._gameView.setController(this);

        for (let business of this._gameState.businesses) {
            this._gameView.addBusiness(business);
        }
    }

    _updateGameView() {
        this._gameView.showMoney(this._gameState.money);

        for (let business of this._gameState.businesses) {
            this._gameView.updateBusiness(business, this._gameState.money);
        }
    }

    _persist() {
        game.storage.persistGameState(this._gameState.serialize());
    }
}
