import 'pixi';
import 'p2';
import 'phaser';

import init from 'states/init'
import load from 'states/load'
import play from 'states/play'

window.game = new Phaser.Game(1280, 720, Phaser.AUTO, 'canvas', null);
game.antialias = false;

game.state.add('init', init);
game.state.add('load', load);
game.state.add('play', play);

game.state.start('init');
