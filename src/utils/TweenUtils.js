export function tweenAlpha(obj, val = 1.0, time = 100) {
    game.add.tween(obj).to({ alpha: val }, time, Phaser.Easing.Quadratic.In, true);
}

export function tweenScale(obj, val = 1.0, time = 500) {
    game.add.tween(obj.scale).to({ x: val, y: val }, time, Phaser.Easing.Quadratic.In, true);
}

export function tweenJump(obj, h = 3, time = 100) {
    game.add.tween(obj).to({ y: obj.y - h }, time, Phaser.Easing.Bounce.InOut, true, 200, 1, true);
}

export function tweenWobble(obj, h = 3 * Math.sign(game.rnd.normal()), time = 150) {
    obj.angle = 0;
    game.add.tween(obj).to({ angle: obj.angle + h }, time, Phaser.Easing.Cubic.Out, true, 0, 0, true);
}
