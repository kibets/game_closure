export default class SoundManager {

    constructor() {
        this._sounds = {};

        this.add('ring', ['ring_01'], 100);
        this.add('plant', ['plant_01'], 100);
        this.add('upgrade', ['upgrade_01'], 100);
        this.add('click', ['click_01', 'click_02', 'click_03'], 100);
    }

    add(group, sounds = [group], cooldown = 500) {
        this._sounds[group] = {
            sounds: sounds.map(name => this._addSound(name, group)),
            cooldown: cooldown,
            canPlay: true
        }
    }

    play(group) {
        if (this._sounds[group].canPlay) {
            game.rnd.pick(this._sounds[group].sounds).play();
        }
    }

    _addSound(name, group) {
        let sound = game.add.sound(name);
        sound.onPlay.add(() => {
            this._sounds[group].canPlay = false;
            game.time.events.add(this._sounds[group].cooldown, () => {
                this._sounds[group].canPlay = true;
            });
        });
        return sound;
    }

}
