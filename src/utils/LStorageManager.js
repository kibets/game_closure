const VERSION = 1;
const KEY_PROGRESS = "plant_a_tree.progress";

export default class LStorageManager {
    constructor() {

    }

    persistGameState(json) {
        if (storageDisabled()) {
            return;
        }
        json.version = VERSION;
        localStorage.setItem(KEY_PROGRESS, JSON.stringify(json));
    }

    loadGameState() {
        if (storageDisabled()) {
            return null;
        }
        let state = localStorage.getItem(KEY_PROGRESS);

        if (state === null || state === undefined) {
            return null;
        }

        try {
            state = JSON.parse(state);
        } catch (e) {
            console.error("LStorage: cannot parse key: " + KEY_PROGRESS, e);
            return null;
        }

        if (state.version !== VERSION) {
            console.warn("LStorage: wrong version for key: " + KEY_PROGRESS + "; expected: " + VERSION);
            return null;
        }
        return state;
    }
}

export function storageDisabled() {
    try {
        localStorage.getItem("none");
        return false;
    } catch (e) {
        return true;
    }
}
