export default class MusicManager {
    constructor() {
        this._music = {};

        this.add('forest');
    }

    add(key) {
        this._music[key] = game.add.sound(key);
    }

    loop(key) {
        this._music[key].play(undefined, undefined, 1, true);
    }
}
