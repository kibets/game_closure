export function toMmSs(ms) {
    if (isNaN(ms) || ms === null || ms < 0) return "--:--";
    let seconds = ms / 1000;
    let pad = (input) => {
        return input < 10 ? "0" + input : input;
    };
    return [
        pad(Math.floor(seconds / 60)),
        pad(Math.floor(seconds % 60)),
    ].join(':');
}
