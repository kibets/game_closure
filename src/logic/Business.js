export default class Business {
    constructor(config) {
        this._config = config;

        // config load
        this._delayBase = config["delayBase"] || 1000;
        this._delayUpgradeScheme = config["delayUpgradeScheme"] || [5, 10, 25, 50, 100];
        this._delayUpgradeMul = config["delayUpgradeMul"] || 0.5;
        this._incomeBase = config["incomeBase"] || 1.0;
        this._upgradeCostBase = config["upgradeCostBase"] || 3.0;
        this._upgradeCostMul = config["upgradeCostMul"] || 1.07;
        this._unlockCost = config["unlockCost"] || 100;
        this._automationCost = config["automationCost"] || 100;

        this.resetState();
    }

    resetState() {
        // actual state:
        this._startTimeMs = null;
        this._level = 1;
        this._unlocked = this._config["unlocked"] || false;
        this._automated = false;

        // calculated multipliers
        this._delayMods = 1.0;

        // denormalized state
        this._lastTimeRemaining = null;
    }

    serialize() {
        return {
            startTimeMs: this._startTimeMs,
            level: this._level,
            unlocked: this._unlocked,
            automated: this._automated,
            delayMods: this._delayMods
        }
    }

    // unsafe
    deserialize(json) {
        this._startTimeMs = json.startTimeMs;
        this._level = json.level;
        this._unlocked = json.unlocked;
        this._automated = json.automated;
        this._delayMods = json.delayMods;
    }

    get businessId() {
        return this._config["id"];
    }

    get level() {
        return this._level;
    }

    get viewInfo() {
        return {
            mainTint: this._config["tint"],
            displayName: this._config["name"]
        }
    }

    get unlockPrice() {
        return this._unlockCost;
    }

    get upgradePrice() {
        return this._calcUpgradePrice();
    }

    get automationPrice() {
        return this._automationCost;
    }

    get isActive() {
        return this._startTimeMs !== null;
    }

    get isUnlocked() {
        return this._unlocked;
    }

    get isAutomated() {
        return this._automated;
    }

    get lastTimeRemaining() {
        return this._lastTimeRemaining;
    }

    unlock() {
        this._unlocked = true;
    }

    automate() {
        this._automated = true;
    }

    startProduction(timeMs) {
        this._startTimeMs = timeMs
    }

    monetizeProduction(timeMs) {
        if (timeMs < this._startTimeMs) {
            // todo: wrong time error
            return 0;
        }
        if (!this._unlocked) {
            // todo: wrong state error
            return 0;
        }
        if (this._startTimeMs === null) {
            return 0;
        }

        let activeDelay = this.calcActualDelay();
        let product = 0;

        if (this._automated) {
            product = Math.floor((timeMs - this._startTimeMs) / activeDelay);
            this._startTimeMs = this._startTimeMs + product * activeDelay;

            this._lastTimeRemaining = timeMs - this._startTimeMs;
        } else {
            if (timeMs - this._startTimeMs >= activeDelay) {
                product = 1;
                this._startTimeMs = null;
                this._lastTimeRemaining = null;
            } else {
                this._lastTimeRemaining = timeMs - this._startTimeMs;
            }
        }
        return product * this.actualIncome;
    }

    get actualIncome() {
        return this._incomeBase * this._level;
    }

    calcActualDelay() {
        return this._delayBase * this._delayMods;
    }

    upgrade() {
        // todo: throw an error if need to monetize first!

        this._level += 1;

        let cyclicLevel = this._delayUpgradeScheme[this._delayUpgradeScheme.length - 1] || Number.MAX_SAFE_INTEGER;

        if (this._delayUpgradeScheme.includes(this._level) || (this._level > cyclicLevel && this._level % cyclicLevel === 0)) {
            this._delayMods *= this._delayUpgradeMul;
        }
    }

    _calcUpgradePrice() {
        return Math.ceil(this._upgradeCostBase * Math.pow(this._upgradeCostMul, this._level - 1));
    }
}
