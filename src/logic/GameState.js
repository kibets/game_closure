import Business from "./Business";

export default class GameState {
    constructor(configs) {
        this._businesses = {};

        for (let config of configs) {
            this._businesses[config.id] = new Business(config);
        }

        this._money = 0;
    }

    serialize() {
        let result = { money: this._money, businesses: {} };

        for (let [id, business] of Object.entries(this._businesses)) {
            result.businesses[id] = business.serialize();
        }
        return result;
    }

    // unsafe
    deserialize(json) {
        if (json === null) {
            return;
        }

        this._money = json.money;
        for (let [id, businessJson] of Object.entries(json.businesses)) {
            this._businesses[id].deserialize(businessJson);
        }
    }

    unlockBusiness(id) {
        if (!this._businesses[id].isUnlocked && this._money >= this._businesses[id].unlockPrice) {
            this._money -= this._businesses[id].unlockPrice;
            this._businesses[id].unlock();
        }
    }

    upgradeBusiness(id) {
        if (this._businesses[id].isUnlocked && this._money >= this._businesses[id].upgradePrice) {
            this._money -= this._businesses[id].upgradePrice;
            this._businesses[id].upgrade();
        }
    }

    automateBusiness(id) {
        if (this._businesses[id].isUnlocked && !this._businesses[id].isAutomated && this._money >= this._businesses[id].automationPrice) {
            this._money -= this._businesses[id].automationPrice;
            this._businesses[id].automate();

            if (!this._businesses[id].isActive) {
                this._businesses[id].startProduction(this.timeMs());
            }
        }
    }

    startBusiness(id) {
        if (this._businesses[id].isUnlocked && !this._businesses[id].isActive) {
            this._businesses[id].startProduction(this.timeMs());
        }
    }

    simulateNow() {
        let time = this.timeMs();

        for (let business of this.businesses) {
            let income = business.monetizeProduction(time);

            this._money += income;
        }
    }

    resetGameProgress() {
        this._money = 0;

        for (let business of this.businesses) {
            business.resetState();
        }
    }

    timeMs() {
        return Date.now(); // todo: better time fn!
    }

    get money() {
        return this._money;
    }

    get businesses() {
        return Object.values(this._businesses);
    }
}
