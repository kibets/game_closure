import { toMmSs } from "../utils/TimeUtils";
import { tweenAlpha } from "../utils/TweenUtils";

export default class ItemProgress extends Phaser.Group {
    constructor(x, y, id, viewInfo, controller) {
        super(game);
        this.x = x;
        this.y = y;

        this._id = id;
        this._controller = controller;

        this._ringProgress = game.make.sprite(0, 0, 'item_frame_f');
        this._ringProgress.tint = viewInfo.mainTint;
        this._ringProgress.alpha = 0.5;
        this._ringProgress.anchor.set(0.5);
        this._ringProgress.scale.set(0.001);
        this.add(this._ringProgress);

        this._timerText = game.make.text(0, -50, '', { fill: '#000000', font: '18pt Cambria' });
        this._timerText.anchor.set(0.5);
        this.add(this._timerText);
    }

    showTimer() {
        tweenAlpha(this._timerText, 0.8);
    }

    hideTimer() {
        tweenAlpha(this._timerText, 0);
    }

    setProgress(timeActual, timeTotal) {
        let time = timeActual === null ? timeTotal : timeTotal - timeActual;

        let percentage = (timeActual / timeTotal) || 0.001;
        this._ringProgress.scale.set(percentage);

        this._timerText.text = timeTotal >= 1000 ? toMmSs(time) : "" + timeTotal + " ms."; // todo: i18n
    }
}
