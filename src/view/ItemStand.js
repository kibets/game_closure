import numberScale from "number-scale";
import { tweenAlpha, tweenJump } from "../utils/TweenUtils";

export default class ItemStand extends Phaser.Group {
    constructor(x, y, id, viewInfo, controller) {
        super(game);
        this.x = x;
        this.y = y;

        this._id = id;
        this._controller = controller;

        this._grass = game.make.sprite(0, 0, 'grass_03');
        this._grass.anchor.set(0.5, 1);
        this.add(this._grass);

        this._stand = game.make.sprite(0, 0, 'item_stand');
        this._stand.anchor.set(0.5);
        this.add(this._stand);

        this._frame = game.make.sprite(0, 0, 'item_stand_f');
        this._frame.tint = viewInfo.mainTint;
        this._frame.anchor.set(0.5);
        this.add(this._frame);

        this._nameText = game.make.text(0, 12, viewInfo.displayName, { fill: '#494602', font: '18pt Cambria' });
        this._nameText.anchor.set(0.5);
        this.add(this._nameText);

        this._levelText = game.make.text(0, -10, '', { fill: '#494602', font: '18pt Cambria' });
        this._levelText.anchor.set(0.5);
        this.add(this._levelText);

        this._upgradeText = game.make.text(0, 14, '', { fill: '#FFFFFF', font: '16pt Cambria' });
        this._upgradeText.anchor.set(0.5);
        this.add(this._upgradeText);

        this._arrowL = game.make.sprite(-86, 10, 'arrow');
        this._arrowR = game.make.sprite(86, 10, 'arrow');
        this._arrowL.anchor.set(0.5);
        this._arrowR.anchor.set(0.5);
        this.add(this._arrowL);
        this.add(this._arrowR);

        this._arrowL.alpha = 0;
        this._arrowR.alpha = 0;
        this._arrowL.tint = 0x92C566;
        this._arrowR.tint = 0x92C566;
        this._stand.events.onInputDown.add(this._upgradeAction, this);

        this._wasAvailable = null;
    }

    unlock() {
        this._nameText.visible = false;
    }

    setUpgradeAvailable(available) {
        if (this._wasAvailable === available) {
            return;
        }
        this._wasAvailable = available;

        if (available) {
            this._stand.inputEnabled = true;
            this._stand.input.useHandCursor = true;
            tweenAlpha(this._arrowL, 1.0);
            tweenAlpha(this._arrowR, 1.0);

            tweenJump(this._arrowL);
            tweenJump(this._arrowR);

            this._upgradeText.fill = "#336633";
        } else {
            this._stand.inputEnabled = false;
            tweenAlpha(this._arrowL, 0);
            tweenAlpha(this._arrowR, 0);
            this._upgradeText.fill = "#663333";
        }
    }

    setLevelText(level) {
        this._levelText.text = "" + level;
    }

    setUpgradeText(value) {
        this._upgradeText.text = "upgrade: " + numberScale(value); // todo: i18n
    }

    _upgradeAction() {
        this._controller.upgradeBusiness(this._id);

        game.sounder.play('upgrade');
    }
}
