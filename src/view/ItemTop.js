import numberScale from "number-scale";
import { tweenAlpha } from "../utils/TweenUtils";

export default class ItemTop extends Phaser.Group {
    constructor(x, y, id, viewInfo, controller) {
        super(game);
        this.x = x;
        this.y = y;

        this._id = id;
        this._controller = controller;

        this._top = game.make.sprite(0, 0, 'item_top');
        this._top.anchor.set(0.5);
        this.add(this._top);

        this._frame = game.make.sprite(0, 0, 'item_top_f');
        this._frame.tint = viewInfo.mainTint;
        this._frame.anchor.set(0.5);
        this.add(this._frame);

        this._incomeText = game.make.text(0, 2, '', { fill: '#5c7715', font: '18pt Cambria' });
        this._incomeText.anchor.set(0.5);
        this.add(this._incomeText);

        this._automateText = game.make.text(0, -34, '', { fill: '#5c7715', font: '18pt Cambria' });
        this._automateText.anchor.set(0.5);
        this.add(this._automateText);

        this._automateBtn = game.make.sprite(0, -34);
        this._automateBtn.width = 200;
        this._automateBtn.height = 20;

        this._automateBtn.anchor.set(0.5);
        this.add(this._automateBtn);

        this._automateBtn.events.onInputDown.add(this._automateAction, this);

        this._automateAvailable = null;

        this.alpha = 0; // hidden by default
    }

    unlock() {
        tweenAlpha(this, 1.0, 300);
    }

    setIncomeText(value) {
        this._incomeText.text = "" + numberScale(value);
    }

    setAutomateAvailable(available, price) {
        if (this._automateAvailable === available) return;

        this._automateAvailable = available;

        if (available) {
            this._automateBtn.inputEnabled = true;
            this._automateBtn.input.useHandCursor = true;
            this._automateText.visible = true;
        } else {
            this._automateBtn.inputEnabled = false;
            this._automateText.visible = false;
        }


        this._automateText.text = 'auto-harvest! (' + numberScale(price) + ')'; // todo: i18n
    }

    _automateAction() {
        this._controller.automateBusiness(this._id);

        game.sounder.play("click");
    }
}
