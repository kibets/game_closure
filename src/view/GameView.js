import BusinessView from "./BusinessView";
import numberScale from 'number-scale';

const ITEM_PAD = 60;
const ITEM_SIZE = 250;

const COLS = 4;

const LOGO_TOP = 40;
const MONEY_TOP = 80;

export default class GameView extends Phaser.Group {
    constructor(x, y) {
        super(game);
        this.x = x;
        this.y = y;

        this._logo = game.make.sprite(game.canvas.width / 2, LOGO_TOP, 'logo');
        this._logo.scale.set(0.5);
        this._logo.anchor.set(0.5);
        this.add(this._logo);

        this._moneyText = game.make.text(game.canvas.width / 2, MONEY_TOP, '0', { fill: '#4a8144' });
        this._moneyText.anchor.set(0.5);
        this.add(this._moneyText);

        this._resetText = game.make.text(game.canvas.width / 2, game.canvas.height - 8, 'RESET ALL', { font: "12pt" }); // todo: i18n
        this._resetText.alpha = 0.5;
        this._resetText.anchor.set(0.5);
        this.add(this._resetText);

        this._resetText.inputEnabled = true;
        this._resetText.input.useHandCursor = true;
        this._resetText.events.onInputDown.add(this._resetGameAction, this);

        this._col = 1;
        this._row = 1;

        this._items = {};
    }

    setController(controller) {
        this._controller = controller;
    }

    addBusiness(business) {
        let itemW = ITEM_SIZE + ITEM_PAD;

        let marginL = (game.canvas.width - itemW * COLS) / 2;
        let marginT = MONEY_TOP + 10;

        let itemX = marginL + itemW / 2 + itemW * (this._col - 1);
        let itemY = marginT + itemW / 2 + itemW * (this._row - 1);

        let item = new BusinessView(itemX, itemY, business.businessId, business.viewInfo, this._controller);
        this.add(item);

        this._items[business.businessId] = item;

        this._col += 1;
        if (this._col > COLS) {
            this._col = 1;
            this._row += 1;
        }
    }

    showMoney(value) {
        this._moneyText.text = "SEEDS: " + numberScale(value); // todo: i18n
    }

    updateBusiness(business, money) {
        this._items[business.businessId].updateFrom(business, money);
    }

    _resetGameAction() {
        this._controller.resetGameProgress();
        location.reload();
    }
}
