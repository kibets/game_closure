import numberScale from "number-scale";

export default class ItemUnlock extends Phaser.Group {
    constructor(x, y, id, viewInfo, controller) {
        super(game);
        this.x = x;
        this.y = y;

        this._id = id;
        this._controller = controller;

        this._unlockable = false;

        this._unlockBtn = game.make.sprite(0, 0, 'item_frame');
        this._unlockBtn.alpha = 0.2;
        this._unlockBtn.tint = 0x000000;
        this._unlockBtn.anchor.set(0.5);
        this.add(this._unlockBtn);

        this._unlockText = game.make.text(0, 0, '---', { fill: '#FFFFFF' });
        this._unlockText.anchor.set(0.5);
        this.add(this._unlockText);

        this._unlockBtn.inputEnabled = true;
        this._unlockBtn.events.onInputDown.add(this._unlockAction, this);
    }

    showAsLocked(canUnlock, unlockPrice) {
        this._unlockText.text = "PLANT: " + numberScale(unlockPrice); // todo: i18n

        if (canUnlock) {
            if (!this._unlockable) {
                game.sounder.play('ring');
            }

            this._unlockText.tint = 0xd9ff00;
            this._unlockable = true;
        } else {
            this._unlockable = false;
            this._unlockText.tint = 0xFF5555;
        }
    }

    unlock() {
        this._unlockText.visible = false;
        this._unlockBtn.visible = false;
        this._unlockBtn.inputEnabled = false;
    }

    _unlockAction() {
        this._controller.unlockBusiness(this._id);

        if (this._unlockable) {
            game.sounder.play("plant");
        }
    }
}
