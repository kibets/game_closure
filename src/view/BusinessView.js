import numberScale from 'number-scale';
import ItemStand from "./ItemStand";
import { tweenScale, tweenWobble } from "../utils/TweenUtils";
import ItemTop from "./ItemTop";
import ItemProgress from "./ItemProgress";
import ItemUnlock from "./ItemUnlock";

export default class BusinessView extends Phaser.Group {
    constructor(x, y, id, viewInfo, controller) {
        super(game);
        this.x = x;
        this.y = y;
        this._id = id;
        this._controller = controller;

        this._grown = false;
        this._clickable = false;

        this._mainTint = viewInfo.mainTint;

        this._circle = game.make.sprite(0, 0, 'item_frame');
        this._circle.anchor.set(0.5);
        this.add(this._circle);

        this._ring = game.make.sprite(0, 0, 'item_frame_f');
        this._ring.tint = this._mainTint;
        this._ring.anchor.set(0.5);
        this.add(this._ring);

        this._tree = game.make.sprite(0, 100, id);
        this._tree.anchor.set(0.5, 1);
        this._tree.scale.set(0.2);
        this.add(this._tree);

        this._itemProgress = new ItemProgress(0, 0, id, viewInfo, controller);
        this.add(this._itemProgress);

        this._itemUnlock = new ItemUnlock(0, 0, id, viewInfo, controller);
        this.add(this._itemUnlock);

        this._itemStand = new ItemStand(0, 110, id, viewInfo, controller);
        this.add(this._itemStand);

        this._itemTop = new ItemTop(0, -104, id, viewInfo, controller);
        this.add(this._itemTop);

        this._circle.inputEnabled = true;
        this._circle.events.onInputDown.add(this._moneyAction, this);
        this._circle.events.onInputOver.add(this._hoverIn, this);
        this._circle.events.onInputOut.add(this._hoverOut, this);

    }

    updateFrom(business, money) {
        if (!business.isUnlocked) {
            this._itemUnlock.showAsLocked(money >= business.unlockPrice, business.unlockPrice);
            return;
        } else {
            if (!this._grown) {
                this._grown = true;
                this.unlock();
            }
        }

        if (!business.isActive && !business.isAutomated) {
            this._clickable = true;
        } else {
            this._clickable = false;
        }

        this._itemStand.setLevelText(business.level);
        this._itemStand.setUpgradeText(business.upgradePrice);
        this._itemStand.setUpgradeAvailable(money >= business.upgradePrice);

        this._itemProgress.setProgress(business.lastTimeRemaining, business.calcActualDelay());

        let automateAvailable = !business.isAutomated && money >= business.automationPrice;
        this._itemTop.setAutomateAvailable(automateAvailable, business.automationPrice);
        this._itemTop.setIncomeText(business.actualIncome);
    }

    unlock() {
        this._itemUnlock.unlock();
        this._itemTop.unlock();
        this._itemStand.unlock();

        tweenScale(this._tree);
    }

    _moneyAction() {
        this._controller.startBusiness(this._id);

        if (this._clickable) {
            game.sounder.play("click");
            tweenWobble(this._tree);
        }
    }

    _hoverIn() {
        this._itemProgress.showTimer();
        this._ring.tint = 0xCBE53C;
    }

    _hoverOut() {
        this._itemProgress.hideTimer();
        this._ring.tint = this._mainTint;
    }
}
