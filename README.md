# Plant A Tree sim game

Play online: https://icxon.itch.io/plant-a-tree

Uses Phaser JS 2.x engine, https://phaser.io

_________________________________________

`npm run dev` - run local server (localhost:8080)

`npm run build` - make a deployment build
